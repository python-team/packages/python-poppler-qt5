python-poppler-qt5 (21.3.0-3) unstable; urgency=medium

  * Bump Standards-Version to 4.6.2 (no change)
  * Rebuild for 64-bit time_t transition:
    libpoppler-qt5-1 has been replaced with libpoppler-qt5-1t64
  * Replace Build-Depends: pkg-config with pkgconf

 -- Anthony Fok <foka@debian.org>  Mon, 04 Mar 2024 09:34:52 -0700

python-poppler-qt5 (21.3.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 29 Nov 2022 16:40:45 +0000

python-poppler-qt5 (21.3.0-1) unstable; urgency=medium

  * New upstream version 21.3.0
  * debian/gbp.conf: Set debian-branch to debian/master for DEP-14 conformance
  * Bump debhelper dependency to "Build-Depends: debhelper-compat (= 13)"
  * Use dh-sequence-python3 instead of dh-python and --with python3
  * Reorder fields in debian/control and debian/copyright
  * Bump Standards-Version to 4.6.1 (no change)
  * Remove sip_{api,buildsystem}.patch which have been merged upstream
  * /usr/lib/python3.10/site-packages/python_poppler_qt5-0.75.0.dist-info/RECORD
    or anything similar is no longer found after a Debian package rebuild.
    Was it a temporary glitch in the binNMU of 0.75.0-3+b1 on 2021-11-20?
    Thanks to Julian Gilbey for the heads-up!  (Closes: #1012188)

 -- Anthony Fok <foka@debian.org>  Tue, 07 Jun 2022 08:45:10 -0600

python-poppler-qt5 (0.75.0-3) unstable; urgency=medium

  * Team upload.
  * Build-depend on sip-tools instead of sip5-tools, to support SIP v6.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 15 Aug 2021 14:55:25 +0300

python-poppler-qt5 (0.75.0-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Dmitry Shachnev ]
  * Build with sip5 instead of sip4 (closes: #964131, #971217).

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 01 Oct 2020 15:49:01 +0300

python-poppler-qt5 (0.75.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Anthony Fok ]
  * New upstream version 0.75.0
  * Run "wrap-and-sort -a"
  * Apply "cme fix dpkg" fixes:
    - Organize debian/control fields
    - Use "Build-Depends: debhelper-compat (= 12)"
    - Bump Standards-Version to 4.5.0 (no change)
  * Add "Rules-Requires-Root: no" to debian/control
  * Add "Testsuite: autopkgtest-pkg-python" with import name popplerqt5

 -- Anthony Fok <foka@debian.org>  Thu, 05 Mar 2020 23:12:02 -0700

python-poppler-qt5 (0.24.2-3) unstable; urgency=high

  * Fix a typo of "highlight" in poppler-annotation.sip
  * Rebuild to fix a hidden (?) ABI incompatibility, probably with
    the recent upgrade to qtbase-abi-5-7-1.

 -- Anthony Fok <foka@debian.org>  Thu, 15 Dec 2016 16:13:18 -0700

python-poppler-qt5 (0.24.2-2) unstable; urgency=high

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Anthony Fok ]
  * Cherry upstream fixes since the 0.24.2 release
  * Refresh debian/control
    - Bump Standards-Version to 3.9.8
    - Tweak Vcs-Browser according to https://wiki.debian.org/Python/GitPackaging
  * This new build incidentally solves an ABI incompatibility with the
    recently updated SIP library: was sip-py3api-11.2, now sip-py3api-11.3.
    (Closes: #839875, #839870)

 -- Anthony Fok <foka@debian.org>  Wed, 05 Oct 2016 22:25:56 -0600

python-poppler-qt5 (0.24.2-1) unstable; urgency=low

  * Initial upload, borrowing from python-poppler-qt4 packaging.
    (Closes: #798087)

 -- Anthony Fok <foka@debian.org>  Sat, 05 Sep 2015 14:55:29 -0600
