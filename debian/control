Source: python-poppler-qt5
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               libpoppler-qt5-dev,
               pkgconf,
               pyqt5-dev (>= 5.15.1),
               python3-all-dev,
               python3-pyqt5 (>= 5.15.1),
               python3-pyqtbuild,
               python3-setuptools,
               qtbase5-dev,
               sip-tools
Testsuite: autopkgtest-pkg-python
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-poppler-qt5
Vcs-Git: https://salsa.debian.org/python-team/packages/python-poppler-qt5.git
Homepage: https://github.com/frescobaldi/python-poppler-qt5

Package: python3-poppler-qt5
Architecture: any
Depends: python3-pyqt5 (>= 5.15.1),
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: Python binding to Poppler-Qt5 C++ library (Python 3)
 Python binding to libpoppler-qt5 that aims for completeness and for being
 actively maintained.  The bindings closely follow the C++ library API
 documented at http://people.freedesktop.org/~aacid/docs/qt5/ .
 .
 This package contains the Python 3 binding.
